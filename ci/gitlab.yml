# CI for `string-interpolate' is deliberately designed to
# mimic the experience that users of the library will have,
# at the expense of reproducibility. This means that it's
# entirely possible for CI builds to start failing even if
# we haven't changed any code at all. That's unfortunate, but
# ultimately our goal is to make sure that this builds on
# everyone's machine, not just 'some' machine that we have
# control over. Assuming we've set up our upper bounds correctly,
# that pathological situation should hopefully never happen.
#
# It looks like it's possible to build this with GHC 8.0.2 and
# 8.2.2, but the images available have the wrong version of cabal
# installed, so it seems to be crapping out.
#
# If we built versions of the Docker images with updated versions
# of Cabal, it would likely work, but for now, we're just leaving
# this as is.

variables:
  GIT_SUBMODULE_STRATEGY: recursive

cache:
  key: "$CI_JOB_IMAGE"
  paths:
    - dist-newstyle
    - .cabaldb

stages:
  - build
  - build-bench
  - test

before_script:
  - 'echo "  ghc-options: -Werror" >> cabal.project'
  - 'echo "store-dir: .cabaldb" >> cabal.project.local'

# build-ghc8.0.2:
#   stage: build
#   image: haskell:8.0.2
#   script:
#     - cabal update
#     - cabal build

# build-ghc8.2.2:
#   stage: build
#   image: haskell:8.2.2
#   script:
#     - cabal update
#     - cabal build

build-ghc8.4.4:
  stage: build
  image: haskell:8.4.4
  script:
    - cabal v2-update
    - cabal v2-build -O0

build-ghc8.6.5:
  stage: build
  image: haskell:8.6.5
  script:
    - cabal v2-update
    - cabal v2-build -O0

build-ghc8.8.4:
  stage: build
  image: haskell:8.8.4
  script:
    - cabal v2-update
    - cabal v2-build -O0

build-ghc8.10.4:
  stage: build
  image: haskell:8.10.4
  script:
    - cabal v2-update
    - cabal v2-build -O0

build-ghc9.0.1:
  stage: build
  image: haskell:9.0.1
  script:
    - cabal v2-update
    - cabal v2-build -O0

# test-ghc8.0.2:
#   stage: test
#   image: haskell:8.0.2
#   script:
#     - cabal update
#     - cabal test

# test-ghc8.2.2:
#   stage: test
#   image: haskell:8.2.2
#   script:
#     - cabal update
#     - cabal test

build-bench-ghc8.4.4:
  stage: build-bench
  image: haskell:8.4.4
  script:
    - cabal v2-update
    - cabal v2-build -O0 -fextended-benchmarks --enable-benchmarks string-interpolate-bench
  dependencies:
    - build-ghc8.4.4

# build-bench-ghc8.6.5:
#   stage: build-bench
#   image: haskell:8.6.5
#   script:
#     - cabal v2-update
#     - cabal v2-build -O0 -fextended-benchmarks --enable-benchmarks string-interpolate-bench
#   dependencies:
#     - build-ghc8.6.5

build-bench-ghc8.8.4:
  stage: build-bench
  image: haskell:8.8.4
  script:
    - cabal v2-update
    - cabal v2-build -O0 -fextended-benchmarks --enable-benchmarks string-interpolate-bench
  dependencies:
    - build-ghc8.8.4

build-bench-ghc8.10.4:
  stage: build-bench
  image: haskell:8.10.4
  script:
    - cabal v2-update
    - cabal v2-build -O0 -fextended-benchmarks --enable-benchmarks string-interpolate-bench
  dependencies:
    - build-ghc8.10.4

build-bench-ghc9.0.1:
  stage: build-bench
  image: haskell:9.0.1
  script:
    - cabal v2-update
    - cabal v2-build -O0 -fextended-benchmarks --enable-benchmarks string-interpolate-bench
  dependencies:
    - build-ghc9.0.1

test-ghc8.4.4:
  stage: test
  image: haskell:8.4.4
  script:
    - cabal v2-update
    - cabal v2-run -O0 --enable-tests string-interpolate-test
  dependencies:
    - build-ghc8.4.4

test-ghc8.6.5:
  stage: test
  image: haskell:8.6.5
  script:
    - cabal v2-update
    - cabal v2-run -O0 --enable-tests string-interpolate-test
  dependencies:
    - build-ghc8.6.5

test-ghc8.8.4:
  stage: test
  image: haskell:8.8.4
  script:
    - cabal v2-update
    - cabal v2-run -O0 string-interpolate-test
  dependencies:
    - build-ghc8.8.4

test-ghc8.10.4:
  stage: test
  image: haskell:8.10.4
  script:
    - cabal v2-update
    - cabal v2-run -O0 string-interpolate-test
  dependencies:
    - build-ghc8.10.4

test-ghc9.0.1:
  stage: test
  image: haskell:9.0.1
  script:
    - cabal v2-update
    - cabal v2-run -O0 string-interpolate-test
  dependencies:
    - build-ghc9.0.1

test-builder-flags:
  stage: test
  image: haskell:8.8.4
  script:
    - cabal v2-update
    - cabal v2-run -O0 --enable-tests --flags "+text-builder +bytestring-builder" string-interpolate-test
  dependencies:
    - build-ghc8.8.4
